##### 关于Apple ID
### 如何创建美洲区 Apple ID?
#### 创建美洲区 Apple ID 的方法
- 请参照[ 创建台湾区 Apple ID ](/LadderFree/kkDictionary/kkAppLadder/iOS/CreatAppleIDofTaiwan.md)的方法做
- 区别是：
     - 在图P07所示标红处
         - 选择<strong> 美国 </strong>即可
     - 在图P18所示标红处
         - 地址：2851 Junction Avenue
         - 城市：San Jose
         - 州：CA 
         - 邮编：95134
         - 国家：U.S.A.
         - 电话：1-408-3828000
#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)
- [关于Apple ID](/LadderFree/kkDictionary/kkAppLadder/iOS/AppleIDList.md)


