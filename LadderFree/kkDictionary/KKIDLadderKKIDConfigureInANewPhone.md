###### 玩转金刚梯>金刚字典>
### 想把金刚号配入新手机，如何操作？
分3步走：

- 第1步：[找回金刚号及配套参数](/LadderFree/kkDictionary/TheKKIDsParametersGetBack.md)

- 第2步：[找回配置说明](/LadderFree/A.md)

- 第3步：配入新手机
  - 将[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)及[ 配套参数 ](/LadderFree/kkDictionary/KKIDsParameters.md)严格按照原样逐字[ 正确录入 ](/LadderFree/kkDictionary/ConsiderationsWhileConfigureKKID.md)手机

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

