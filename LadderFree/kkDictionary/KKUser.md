###### 玩转金刚>金刚字典>

### 金刚用户

- 凡使用[ 金刚VPN产品 ](/LadderFree/A.md)和[ 金刚VPN服务 ](/LadderFree/kkDictionary/KKServices.md)的自然人、公司、非公司组织都是<Strong> 金刚用户 </Strong>
- 使用[ 金刚VPN产品 ](/LadderFree/A.md)和[ 金刚VPN服务 ](/LadderFree/kkDictionary/KKServices.md)前，使用者须与[ 金刚公司 ](/LadderFree/kkDictionary/Atozitpro.md)签署一份[《金刚公司与最终用户合约》](/LadderFree/kkDictionary/KKEnduserContract.md)
- 该[ 合约 ](/LadderFree/kkDictionary/KKEnduserContract.md)签署后，使用者即成为<Strong> 金刚用户 </Strong>

#### 返回到
- [玩转金刚](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

